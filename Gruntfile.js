/* eslint no-undef: 0 */
module.exports = function(grunt) {
  'use strict';
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({

    config: {
      source: 'site',
      dest: 'dist',
      temp: '.tmp'
    },

    readAssetHash: function() {
      return grunt.file.read(".tmp/assetHash");
    },

    eslint: {
      // http://eslint.org/docs/rules/
      target: '<%= config.source %>/assets/js/**/*'
    },

    sasslint: {
      // https://github.com/sasstools/sass-lint/tree/master/docs/rules
      target: '<%= config.source %>/assets/scss/**/*'
    },

    clean: {
      build: {
        files: [{
          dot: true,
          src: [
            '<%= config.temp %>/*',
            '<%= config.dest %>/*'
          ]
        }]
      }
    },

    svgmin: {
      options: {
        js2svg: {
          pretty: true,
          indent: 2,
        },
        plugins: [],
      },
      clean: {
        files: [{
          flatten: false,
          expand: true,
          cwd: '<%= config.source %>/assets/',
          src: ['icons/*.svg', 'img/*.svg'],
          dest: '<%= config.source %>/assets/'
        }]
      }
    },

    sass: {
      options: {
        style: 'compressed',
        sourceMap: true,
        includePaths: [
          '<%= config.source %>/assets/scss/',
          './node_modules/'
        ]
      },
      build: {
        files: {
          '<%= config.temp %>/main.css': '<%= config.source %>/assets/scss/main.scss',
        }
      }
    },

    postcss: {
      options: {
        map: {
          annotation: true,
          sourcesContent: true
        },
        processors: [
          // browser list is managed in package.json
          // http://browserl.ist/?q=defaults
          require('autoprefixer'),
          require('postcss-csso'),
        ]
      },
      dist: {
        src: '<%= config.temp %>/main.css'
      }
    },

    concat: {
      options: {
        stripBanners: true,
        sourceMap: true,
      },

      js: {
        options: { separator: ';\n', },
        src: [
          "node_modules/jquery/dist/jquery.min.js",
          "node_modules/slick-carousel/slick/slick.min.js",
          "<%= config.temp %>/script.min.js"
        ],
        dest: "<%= config.dest %>/assets/main.min.<%= readAssetHash() %>.js",
      },

      css: {
        src: [
          "node_modules/normalize.css/normalize.css",
          "<%= config.temp %>/main.css",
        ],
        dest: "<%= config.dest %>/assets/main.min.<%= readAssetHash() %>.css",
      },
    },

    uglify: {
      options: {
        sourceMap: true,
      },
      main: {
        files: [{
          src: "<%= config.source %>/assets/js/*.js",
          dest: "<%= config.temp %>/script.min.js"
        }]
      },
    },

    copy: {
      assets: {
        files: [{
          expand: true,
          cwd: '<%= config.source %>/assets/',
          src: ['{fonts,img,icons,vendor}/**/*'],
          dest: '<%= config.dest %>/assets/'
        }]
      },
      misc: {
        files: [{
          expand: true,
          cwd: '<%= config.source %>/assets/',
          src: [
            'favicon.ico'
          ],
          dest: '<%= config.dest %>'
        }]
      }
    },

    assemble: {
      options: {
        production: false,
        assets: '<%= config.dest %>/assets',
        assetHash: '<%= readAssetHash() %>',
        data: '<%= config.source %>/data/*',
        flatten: true,
        layout: 'default.hbs',
        layoutdir: '<%= config.source %>/layouts',
        partials: '<%= config.source %>/partials/**/*.hbs'
      },

      buildDev: {
        files: [{'<%= config.dest %>/': ['<%= config.source %>/pages/**/*.hbs']}]
      },

      buildProd: {
        options: { production: true, },
        files: [{'<%= config.dest %>/': ['<%= config.source %>/pages/**/*.hbs']}]
      }
    },

    watch: {
      images: {
        files: '<%= config.source %>/img/**/*',
        tasks: ['copy:assets']
      },
      scss: {
        files: '<%= config.source %>/assets/**/*.scss',
        tasks: ['concurrent:scssWatch']
      },
      js: {
        files: '<%= config.source %>/assets/**/*.{json,js}',
        tasks: ['concurrent:jsWatch']
      },
      assemble: {
        files: [
          '<%= config.source %>/**/*.hbs',
          '<%= config.source %>/data/*',
        ],
        tasks: ['assemble:buildDev']
      }
    },

    browserSync: {
      serve: {
        bsFiles: { src: [ '<%= config.dest %>/**.*' ] },
        options: {
          watchTask: true,
          server: '<%= config.dest %>',
          // browser: ["google chrome"],
          // tunnel: true,
          open: 'local',
          notify: false,
          ghostMode: {
            clicks: true,
            forms: true,
            scroll: true
          }
        }
      }
    },

    'sftp-deploy': {
      build: {
        auth: {
          host: 'flightstar.com',
          port: 8791,
          authKey: 'production'
        },
        cache: 'sftpCache.json',
        src: '<%= config.dest %>/',
        dest: 'www/',
        progress: true
      }
    },

    concurrent: {
      scssWatch: ['sasslint', 'buildScss'],
      jsWatch:   ['eslint',   'buildJs'],
      buildDev:  ['buildJs',  'buildScss', 'assemble:buildDev'],
      buildProd: ['buildJs',  'buildScss', 'assemble:buildProd'],
    }

  });

  grunt.registerTask( 'genAssetHash', function() {
    var filePath = ".tmp/assetHash";
    var crypto = require('crypto');
    var hash = crypto.randomBytes(4).toString('hex');

    if(grunt.file.exists(filePath)){ grunt.file.delete(filePath, {force: true}); }
    grunt.file.write(filePath, hash);
    return hash;
  });

  // Tasks
  grunt.registerTask('svg', ['svgmin:clean']);

  grunt.registerTask('lint',      ['sasslint',    'eslint']);
  grunt.registerTask('preBuild',  ['svgmin:clean', 'clean:build', 'genAssetHash', 'copy']);
  grunt.registerTask('buildJs',   ['uglify', 'concat:js']);
  grunt.registerTask('buildScss', ['sass', 'postcss', 'concat:css']);

  grunt.registerTask('buildDev', ['preBuild', 'concurrent:buildDev']);
  grunt.registerTask('build',    ['preBuild', 'concurrent:buildProd']);

  grunt.registerTask('serve',   ['lint',  'buildDev', 'browserSync', 'watch']);
  grunt.registerTask('deploy',  ['build', 'sftp-deploy:build']);
  grunt.registerTask('default', ['lint',  'build']);
};
